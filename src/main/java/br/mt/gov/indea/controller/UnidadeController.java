package br.mt.gov.indea.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import br.mt.gov.indea.model.Unidade;
import br.mt.gov.indea.service.UnidadeService;

@RestController
public class UnidadeController {
	
	@Autowired
	private UnidadeService unidadeService;
	
	@GetMapping("/unidades")
	public List<Unidade> getAllUnidades(){
		return unidadeService.findAll();
	}
	
	@GetMapping("/unidade/{id}")
	public Unidade getUnidade(@PathVariable Long id){
		return unidadeService.findById(id).get();
	}

}
