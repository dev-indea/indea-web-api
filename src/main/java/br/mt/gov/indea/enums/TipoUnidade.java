package br.mt.gov.indea.enums;

public enum TipoUnidade {

	PRESIDENCIA(1,"Presidencia","Presidencia"),
    DIRETORIA(2, "Diretoria","Diretoria"),
    APOIO(3, "Unidade de Apoio", "Unidade de Apoio"),
    COORDENADORIA(4, "Coordenadoria", "Coordenadoria"),
    GERENCIA(5,"Gerência","Gerência"),
    LABORATORIO(6,"Laboratório","Laboratório"),
    URS(7,"URS","Unidade Regional de Supervisão"),
    ULE(8,"ULE","Unidade Local de Execução"),
    PF(9,"Posto Fiscal","Posto Fiscal"),
    PA(10,"Ponto de Atendimento","Ponto de Atendimento"),
    BARREIRA(11,"Barreira","Barreira");
	
	private long value;
	private String descricao;
	private String longDescricao;

	TipoUnidade(long value, String descricao, String longDescricao) {
		this.value = value;
		this.descricao = descricao;
		this.longDescricao = longDescricao;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getLongDescricao() {
		return longDescricao;
	}

	public void setLongDescricao(String longDescricao) {
		this.longDescricao = longDescricao;
	}
	
}
