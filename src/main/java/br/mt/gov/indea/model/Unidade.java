package br.mt.gov.indea.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.mt.gov.indea.enums.TipoUnidade;

@Entity
public class Unidade implements Serializable {

	private static final long serialVersionUID = 2298728071643989802L;

	@Id
	private Long id;
	
    @Column(nullable = false, length = 110)
    private String nome;
    
    @Column(nullable = false, length = 40)
    private String sigla;
    
    @Column(length = 45)
    private String email;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "data_atualizado")
    private Date dataAtualizado;
    
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "unidade_pai_id")
    private Unidade unidadePai;
    
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "tipo_unidade", nullable = false)
    private TipoUnidade tipoUnidade;
    
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "municipio_id")
    private Municipio municipio;
    
	@Column(name = "cod_seap")
    private Integer codSeap;
    
    private boolean ativo = true;
    
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToMany(mappedBy = "unidade", fetch = FetchType.LAZY)
    private List<Servidor> servidores = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDataAtualizado() {
		return dataAtualizado;
	}

	public void setDataAtualizado(Date dataAtualizado) {
		this.dataAtualizado = dataAtualizado;
	}

	public Unidade getUnidadePai() {
		return unidadePai;
	}

	public void setUnidadePai(Unidade unidadePai) {
		this.unidadePai = unidadePai;
	}

	public TipoUnidade getTipoUnidade() {
		return tipoUnidade;
	}

	public void setTipoUnidade(TipoUnidade tipoUnidade) {
		this.tipoUnidade = tipoUnidade;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Integer getCodSeap() {
		return codSeap;
	}

	public void setCodSeap(Integer codSeap) {
		this.codSeap = codSeap;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public List<Servidor> getServidores() {
		return servidores;
	}

	public void setServidores(List<Servidor> servidores) {
		this.servidores = servidores;
	}
    
    
}
