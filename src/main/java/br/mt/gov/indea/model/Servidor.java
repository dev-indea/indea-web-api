package br.mt.gov.indea.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.mt.gov.indea.enums.TipoPessoa;

@Entity
@DiscriminatorValue("1")
@Table(name = "servidor", schema = "cogesp")
public class Servidor extends Pessoa implements Serializable {
	
	private static final long serialVersionUID = 5094652616748831719L;

	@Id
	private Long id;

    @Column(nullable = false, length = 10)
    private String matricula;
    
    @Column(nullable = false, name = "data_nascimento")
    private LocalDate dataNascimento;
    
    @Column(nullable = false, name = "data_exercicio")
    private Date dataExercicio;
    
    @Column(nullable = false, length = 14)
    private String cpf;
    
	private boolean responsavel = false;
    
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "unidade_id")
    private Unidade unidade;
    
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cargo_id")
    private Cargo cargo;
    
    @Transient
    public TipoPessoa getTipoPessoa(){
        return TipoPessoa.FISICA;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Date getDataExercicio() {
		return dataExercicio;
	}

	public void setDataExercicio(Date dataExercicio) {
		this.dataExercicio = dataExercicio;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public boolean isResponsavel() {
		return responsavel;
	}

	public void setResponsavel(boolean responsavel) {
		this.responsavel = responsavel;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
    
}
