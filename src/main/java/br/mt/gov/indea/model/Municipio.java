package br.mt.gov.indea.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Municipio implements Serializable {

	private static final long serialVersionUID = 1911510396911794957L;

	@Id
    private Long id;
    
    @Column(nullable = false, length = 35)
    private String nome; 
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "uf_id", nullable = false)
    private Uf uf;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
}
