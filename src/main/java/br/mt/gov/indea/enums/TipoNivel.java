package br.mt.gov.indea.enums;

public enum TipoNivel {
	
	NAO_ESPECIFICADO("Nãoo Especificado"),
    FUNDAMENTAL("Fundamental"),
    MEDIO("Médio"),
    SUPERIO("Superior");

    String descricao;

    TipoNivel(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

}
