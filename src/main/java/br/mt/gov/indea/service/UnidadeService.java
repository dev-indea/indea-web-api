package br.mt.gov.indea.service;

import org.springframework.data.jpa.repository.JpaRepository;

import br.mt.gov.indea.model.Unidade;

public interface UnidadeService extends JpaRepository<Unidade, Long>{
	
	public Unidade findByNome(String nome);

}
