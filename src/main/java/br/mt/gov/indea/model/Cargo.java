package br.mt.gov.indea.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import br.mt.gov.indea.enums.TipoNivel;

@Entity
@Table(name = "cargo", schema = "cogesp")
public class Cargo implements Serializable {

	private static final long serialVersionUID = -926803551187886194L;

	@Id
	private Long id;
	
    @Column(nullable = false, length = 110)
    private String nome;
    
    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private TipoNivel nivel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoNivel getNivel() {
		return nivel;
	}

	public void setNivel(TipoNivel nivel) {
		this.nivel = nivel;
	}
	
}
