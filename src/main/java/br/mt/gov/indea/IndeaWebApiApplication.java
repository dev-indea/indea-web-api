package br.mt.gov.indea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndeaWebApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(IndeaWebApiApplication.class, args);
	}

}
