package br.mt.gov.indea.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Uf implements Serializable {
	
	private static final long serialVersionUID = -3281959911959873934L;

	@Id
    private Long id;
    
    @Column(nullable = false, length = 20, unique = true)
    private String nome;
    
    @Column(nullable = false, length = 2)
    private String sigla;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
}
